/**
 * @author gold
 * @version 1.0
 * @created 21-Nov-2016 14:57:39
 */
public class Button {

	private Control control;
	private int whereTo;
	private Control m_Control;

	public Button(){
	}

	public void finalize() throws Throwable {

	}
	/**
	 * 
	 * @param control
	 * @param whereTo
	 */
	public Button(Control control, int whereTo){
	       this.control = control;
	       this.whereTo = whereTo;
	}

	public void push(){
	    switch(whereTo){
	    
	    case 0:
	        control.setZielStockwerk(0);
	        control.onTriggerOccured("TriggerButton0");
	        
	        break;
	    case 1:
	        control.setZielStockwerk(1);
	        control.onTriggerOccured("TriggerButton1");
	        break;
	    case 2:
	        control.setZielStockwerk(2);
	        control.onTriggerOccured("TriggerButton2");
	    }
	}
}//end Button