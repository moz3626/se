
/**
 * @author gold
 * @version 1.0
 * @created 21-Nov-2016 14:57:15
 */
public class Sensor {

	private Control control;
	private int level;
	public Control m_Control;

	public Sensor(){

	}

	public void finalize() throws Throwable {

	}
	/**
	 * 
	 * @param control
	 * @param level
	 */
	public Sensor(Control control, int level){
	    this.control = control;
	    this.level = level;
	}

	public void fire(){
	    switch(level){
	    case 0:
	        control.onTriggerOccured("TriggerSensor0");
	        break;
	    case 1:
	        control.onTriggerOccured("TriggerSensor1");
	        break;
	    case 2:
	        control.onTriggerOccured("TriggerSensor2");
	    }
	}
}//end Sensor