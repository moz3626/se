/**
 * @author gold
 * @version 1.0
 * @created 25-Nov-2016 12:09:21
 */
public class Control {
    
    private int aktuellesStockwerk;
    private int zielStockwerk;
    private boolean moving = false;

	public void setZielStockwerk(int zielStockwerk) {
	    if(!moving){
	        this.zielStockwerk = zielStockwerk;
	    }
    }

    public Control(){
	    aktuellesStockwerk = 0;
	}

	public void finalize() throws Throwable {

	}
	/* Begin - EA generated code for StateMachine */

	private enum StateType
	{
			Elevator_Stehen,
		Elevator_Runterfahren,
		Elevator_Hochfahren,
		ST_NOSTATE
	}
	private enum TransitionType
	{
			Elevator_Stehen_to_Elevator_Hochfahren_BA8CD472_C5FF_4741_B067_60C36B1D795D,
		Elevator_Stehen_to_Elevator_Runterfahren_AFC4D76A_168C_4288_8CAD_E2DD3B57DAD8,
		Elevator_Stehen_to_Elevator_Hochfahren_6E3379FE_D33B_4268_AD8A_B5112D2170A1,
		Elevator_Stehen_to_Elevator_Runterfahren_2D80457C_FCE8_46fb_B32F_F64D6DAAD32B,
		Elevator_Runterfahren_to_Elevator_Runterfahren_61206079_1FF2_4efd_8168_A3F31BCD80B7,
		Elevator_Hochfahren_to_Elevator_Hochfahren_2CED136A_5AF5_4e6d_91F3_160470EE4A2A,
		TT_NOTRANSITION
	}
	private enum CommandType
	{
		Do, 
		Entry, 
		Exit 
	}
	private StateType currState;
	private StateType nextState;
	private TransitionType currTransition;
	private boolean transcend;
	private boolean bStayInCurrentState = false;
		private StateType Elevator_history;	
	private void elevator_Stehen(CommandType command)
	{
		switch(command)
		{
			case Do:
			{
				if(!bStayInCurrentState)
				{
					// Do Behaviors..
				}		
				bStayInCurrentState = true;
				// State's Transitions


				if("TriggerButton2".equals(currentTrigger))
				{
				if(((aktuellesStockwerk < 2) && (aktuellesStockwerk < zielStockwerk)))
				{
					nextState = StateType.Elevator_Hochfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Hochfahren_BA8CD472_C5FF_4741_B067_60C36B1D795D;
				}
				}


				if("TriggerButton0".equals(currentTrigger))
				{
				if(((aktuellesStockwerk > 0) && (aktuellesStockwerk > zielStockwerk)))
				{
					nextState = StateType.Elevator_Runterfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Runterfahren_AFC4D76A_168C_4288_8CAD_E2DD3B57DAD8;
				}
				}


				if("TriggerButton1".equals(currentTrigger))
				{
				if(((aktuellesStockwerk < 2) && (aktuellesStockwerk < zielStockwerk)))
				{
					nextState = StateType.Elevator_Hochfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Hochfahren_6E3379FE_D33B_4268_AD8A_B5112D2170A1;
				}
				}


				if("TriggerButton1".equals(currentTrigger))
				{
				if(((aktuellesStockwerk > 0) && (aktuellesStockwerk > zielStockwerk)))
				{
					nextState = StateType.Elevator_Runterfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Runterfahren_2D80457C_FCE8_46fb_B32F_F64D6DAAD32B;
				}
				}


				if("TriggerButton2".equals(currentTrigger))
				{
				if(((aktuellesStockwerk < 2) && (aktuellesStockwerk < zielStockwerk)))
				{
					nextState = StateType.Elevator_Hochfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Hochfahren_BA8CD472_C5FF_4741_B067_60C36B1D795D;
				}
				}


				if("TriggerButton0".equals(currentTrigger))
				{
				if(((aktuellesStockwerk > 0) && (aktuellesStockwerk > zielStockwerk)))
				{
					nextState = StateType.Elevator_Runterfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Runterfahren_AFC4D76A_168C_4288_8CAD_E2DD3B57DAD8;
				}
				}


				if("TriggerButton1".equals(currentTrigger))
				{
				if(((aktuellesStockwerk < 2) && (aktuellesStockwerk < zielStockwerk)))
				{
					nextState = StateType.Elevator_Hochfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Hochfahren_6E3379FE_D33B_4268_AD8A_B5112D2170A1;
				}
				}


				if("TriggerButton1".equals(currentTrigger))
				{
				if(((aktuellesStockwerk > 0) && (aktuellesStockwerk > zielStockwerk)))
				{
					nextState = StateType.Elevator_Runterfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Runterfahren_2D80457C_FCE8_46fb_B32F_F64D6DAAD32B;
				}
				}


				if("TriggerButton2".equals(currentTrigger))
				{
				if(((aktuellesStockwerk < 2) && (aktuellesStockwerk < zielStockwerk)))
				{
					nextState = StateType.Elevator_Hochfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Hochfahren_BA8CD472_C5FF_4741_B067_60C36B1D795D;
				}
				}


				if("TriggerButton0".equals(currentTrigger))
				{
				if(((aktuellesStockwerk > 0) && (aktuellesStockwerk > zielStockwerk)))
				{
					nextState = StateType.Elevator_Runterfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Runterfahren_AFC4D76A_168C_4288_8CAD_E2DD3B57DAD8;
				}
				}


				if("TriggerButton1".equals(currentTrigger))
				{
				if(((aktuellesStockwerk < 2) && (aktuellesStockwerk < zielStockwerk)))
				{
					nextState = StateType.Elevator_Hochfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Hochfahren_6E3379FE_D33B_4268_AD8A_B5112D2170A1;
				}
				}


				if("TriggerButton1".equals(currentTrigger))
				{
				if(((aktuellesStockwerk > 0) && (aktuellesStockwerk > zielStockwerk)))
				{
					nextState = StateType.Elevator_Runterfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Runterfahren_2D80457C_FCE8_46fb_B32F_F64D6DAAD32B;
				}
				}


				if("TriggerButton2".equals(currentTrigger))
				{
				if(((aktuellesStockwerk < 2) && (aktuellesStockwerk < zielStockwerk)))
				{
					nextState = StateType.Elevator_Hochfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Hochfahren_BA8CD472_C5FF_4741_B067_60C36B1D795D;
				}
				}


				if("TriggerButton0".equals(currentTrigger))
				{
				if(((aktuellesStockwerk > 0) && (aktuellesStockwerk > zielStockwerk)))
				{
					nextState = StateType.Elevator_Runterfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Runterfahren_AFC4D76A_168C_4288_8CAD_E2DD3B57DAD8;
				}
				}


				if("TriggerButton1".equals(currentTrigger))
				{
				if(((aktuellesStockwerk < 2) && (aktuellesStockwerk < zielStockwerk)))
				{
					nextState = StateType.Elevator_Hochfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Hochfahren_6E3379FE_D33B_4268_AD8A_B5112D2170A1;
				}
				}


				if("TriggerButton1".equals(currentTrigger))
				{
				if(((aktuellesStockwerk > 0) && (aktuellesStockwerk > zielStockwerk)))
				{
					nextState = StateType.Elevator_Runterfahren;
					currTransition = TransitionType.Elevator_Stehen_to_Elevator_Runterfahren_2D80457C_FCE8_46fb_B32F_F64D6DAAD32B;
				}
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

	private void elevator_Runterfahren(CommandType command)
	{
		switch(command)
		{
			case Do:
			{
				if(!bStayInCurrentState)
				{
					// Do Behaviors..
				}		
				bStayInCurrentState = true;
				// State's Transitions


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk == aktuellesStockwerk) && (aktuellesStockwerk == 1)))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor0".equals(currentTrigger))
				{
				if((zielStockwerk == aktuellesStockwerk))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk != aktuellesStockwerk) && (aktuellesStockwerk > 0)))
				{
					nextState = StateType.Elevator_Runterfahren;
					currTransition = TransitionType.Elevator_Runterfahren_to_Elevator_Runterfahren_61206079_1FF2_4efd_8168_A3F31BCD80B7;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk == aktuellesStockwerk) && (aktuellesStockwerk == 1)))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor0".equals(currentTrigger))
				{
				if((zielStockwerk == aktuellesStockwerk))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk != aktuellesStockwerk) && (aktuellesStockwerk > 0)))
				{
					nextState = StateType.Elevator_Runterfahren;
					currTransition = TransitionType.Elevator_Runterfahren_to_Elevator_Runterfahren_61206079_1FF2_4efd_8168_A3F31BCD80B7;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk == aktuellesStockwerk) && (aktuellesStockwerk == 1)))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor0".equals(currentTrigger))
				{
				if((zielStockwerk == aktuellesStockwerk))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk != aktuellesStockwerk) && (aktuellesStockwerk > 0)))
				{
					nextState = StateType.Elevator_Runterfahren;
					currTransition = TransitionType.Elevator_Runterfahren_to_Elevator_Runterfahren_61206079_1FF2_4efd_8168_A3F31BCD80B7;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk == aktuellesStockwerk) && (aktuellesStockwerk == 1)))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor0".equals(currentTrigger))
				{
				if((zielStockwerk == aktuellesStockwerk))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk != aktuellesStockwerk) && (aktuellesStockwerk > 0)))
				{
					nextState = StateType.Elevator_Runterfahren;
					currTransition = TransitionType.Elevator_Runterfahren_to_Elevator_Runterfahren_61206079_1FF2_4efd_8168_A3F31BCD80B7;
				}
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

	private void elevator_Hochfahren(CommandType command)
	{
		switch(command)
		{
			case Do:
			{
				if(!bStayInCurrentState)
				{
					// Do Behaviors..
				}		
				bStayInCurrentState = true;
				// State's Transitions


				if("TriggerSensor2".equals(currentTrigger))
				{
				if((zielStockwerk == aktuellesStockwerk))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk == aktuellesStockwerk) && (aktuellesStockwerk == 1)))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk != aktuellesStockwerk) && (aktuellesStockwerk < 2)))
				{
					nextState = StateType.Elevator_Hochfahren;
					currTransition = TransitionType.Elevator_Hochfahren_to_Elevator_Hochfahren_2CED136A_5AF5_4e6d_91F3_160470EE4A2A;
				}
				}


				if("TriggerSensor2".equals(currentTrigger))
				{
				if((zielStockwerk == aktuellesStockwerk))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk == aktuellesStockwerk) && (aktuellesStockwerk == 1)))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk != aktuellesStockwerk) && (aktuellesStockwerk < 2)))
				{
					nextState = StateType.Elevator_Hochfahren;
					currTransition = TransitionType.Elevator_Hochfahren_to_Elevator_Hochfahren_2CED136A_5AF5_4e6d_91F3_160470EE4A2A;
				}
				}


				if("TriggerSensor2".equals(currentTrigger))
				{
				if((zielStockwerk == aktuellesStockwerk))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk == aktuellesStockwerk) && (aktuellesStockwerk == 1)))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk != aktuellesStockwerk) && (aktuellesStockwerk < 2)))
				{
					nextState = StateType.Elevator_Hochfahren;
					currTransition = TransitionType.Elevator_Hochfahren_to_Elevator_Hochfahren_2CED136A_5AF5_4e6d_91F3_160470EE4A2A;
				}
				}


				if("TriggerSensor2".equals(currentTrigger))
				{
				if((zielStockwerk == aktuellesStockwerk))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk == aktuellesStockwerk) && (aktuellesStockwerk == 1)))
				{
					nextState = StateType.Elevator_Stehen;
				}
				}


				if("TriggerSensor1".equals(currentTrigger))
				{
				if(((zielStockwerk != aktuellesStockwerk) && (aktuellesStockwerk < 2)))
				{
					nextState = StateType.Elevator_Hochfahren;
					currTransition = TransitionType.Elevator_Hochfahren_to_Elevator_Hochfahren_2CED136A_5AF5_4e6d_91F3_160470EE4A2A;
				}
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	private void StatesProc(StateType currState, CommandType command)
	{
		switch(currState)
		{
			case Elevator_Stehen:
		{
			elevator_Stehen(command);
			moving =false;
			break;
		}

		case Elevator_Runterfahren:
		{
			elevator_Runterfahren(command);
			moving = true;
			break;
		}

		case Elevator_Hochfahren:
		{
			elevator_Hochfahren(command);
			moving = true;
			break;
		}
			default:
				break;
		}
	}
	private void TransitionsProc(TransitionType transition)
	{
		switch(transition)
		{
			case Elevator_Stehen_to_Elevator_Hochfahren_BA8CD472_C5FF_4741_B067_60C36B1D795D:
		{
			aktuellesStockwerk++;
			break;
		}
		case Elevator_Stehen_to_Elevator_Runterfahren_AFC4D76A_168C_4288_8CAD_E2DD3B57DAD8:
		{
			aktuellesStockwerk--;
			break;
		}
		case Elevator_Stehen_to_Elevator_Hochfahren_6E3379FE_D33B_4268_AD8A_B5112D2170A1:
		{
			aktuellesStockwerk++;
			break;
		}
		case Elevator_Stehen_to_Elevator_Runterfahren_2D80457C_FCE8_46fb_B32F_F64D6DAAD32B:
		{
			aktuellesStockwerk--;
			break;
		}
		case Elevator_Runterfahren_to_Elevator_Runterfahren_61206079_1FF2_4efd_8168_A3F31BCD80B7:
		{
			aktuellesStockwerk--;
			break;
		}
		case Elevator_Hochfahren_to_Elevator_Hochfahren_2CED136A_5AF5_4e6d_91F3_160470EE4A2A:
		{
			aktuellesStockwerk++;
			break;
		}
			default:
				break;
		}
	}
	private void initializeStateMachine()
	{ 
		currState = StateType.Elevator_Stehen;
		nextState = StateType.ST_NOSTATE;
		currTransition = TransitionType.TT_NOTRANSITION;
	} 
	

	private void runStateMachine()
	{
		if( currState == null ) {
			initializeStateMachine();
		}
			
		while(true)
		{
			if ( currState == StateType.ST_NOSTATE )
			{
				break ; 
			}
			
			nextState = StateType.ST_NOSTATE;
			currTransition = TransitionType.TT_NOTRANSITION;
			StatesProc(currState, CommandType.Do);
			// and then check if there is any valid transition assigned after the do behavior
			if ( nextState == StateType.ST_NOSTATE)
			{
				if( currentTrigger == null ) {
					break;
				}
				
				break;
			}
			
			if ( currTransition != TransitionType.TT_NOTRANSITION )
			{
				TransitionsProc( currTransition );
			}
			currentTrigger = null;
			if ( currState != nextState)
			{
				System.out.println("FromState: " + currState.toString() + " --> " +  "ToState: " + nextState.toString() );
				StatesProc(currState, CommandType.Exit);
				StatesProc(nextState, CommandType.Entry);
				currState = nextState ;
			}
			else if ( currState == nextState) {
				System.out.println("FromState: " + currState.toString() + " --> " +  "ToState: " + nextState.toString() );
			}
		}
	}	
	public  java.util.List<String> triggerList = new java.util.ArrayList<String>();
	private String 		 currentTrigger;
	/**
	 * 
	 * @param eventType
	 */
	 
	public void onTriggerOccured( String triggerName ){
		triggerList.add( triggerName );
		while( triggerList.size() > 0 ) {
			currentTrigger = triggerList.remove( 0 );
			nextState = StateType.ST_NOSTATE;
			runStateMachine();
			System.out.println("Aktuelles Stockwerk ist: " + aktuellesStockwerk + "\nmoving ist: " + moving);
		}		
	}
	/* End - EA generated code for StateMachine */
}//end Control