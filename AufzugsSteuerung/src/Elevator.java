import java.util.Scanner;

public class Elevator {
    
    public static void main(String args[]){
        
        Scanner sc = new Scanner(System.in);
        String cmd = "";
        boolean exit = true;
        // Create Control object
        Control control = new Control();
 
        // Buttons
        Button b_ground = new Button(control,0);
        Button b_first = new Button(control,1);
        Button b_second = new Button(control,2);
        
        // Sensors
        Sensor s_ground = new Sensor(control,0);
        Sensor s_first = new Sensor(control,1);
        Sensor s_second = new Sensor(control,2);
        
        // Input command from shell
        System.out.println("Input Trigger: \n");
        while(exit){
            cmd = sc.nextLine().trim();
            switch(cmd){
                case "exit":
                    exit = false;
                    break;
                case "help":
                    System.out.println("Buttons: b_ground, b_first, b_second\nSensors: s_ground, s_first, s_second\n");
                    break;
                case "b_ground":
                    b_ground.push();
                    break;
                case "b_first":
                    b_first.push();
                    break;
                case "b_second":
                    b_second.push();
                    break;
                case "s_ground":
                    s_ground.fire();
                    break;
                case "s_first":
                    s_first.fire();
                    break;
                case "s_second":
                    s_second.fire();
                    break;
                default:
                    System.out.println("for help type help :)\n");
            }
        }
        
        sc.close();
        
        
    }
    
}
