package aufgabe07_2;

public class binarySearch {
	
	public static int binarySearch01(int[] a, int key) {
		int low = 0;
		int high = a.length - 1;
		while (low <= high) {
			int mid = (low + high) >> 1;
			int midVal = a[mid];
			if (midVal < key)
				low = mid + 1;
			else if (midVal > key)
				high = mid - 1;
			else
				return mid; // key found
		}
		return (-low -1); // key not found
	}
	/*
	// Verbesserte Version -> gibt -1 statt fälschlicherweise 0 bei leerem Array aus
	public static int binarySearch02(int[] a, int key) {
		int low = 0;
		int high = a.length - 1;
		while (low <= high) {
			int mid = (low + high) >> 1;
			int midVal = a[mid];
			if (midVal < key)
				low = mid + 1;
			else if (midVal > key)
				high = mid - 1;
			else
				return mid; // key found
		}
		return -1; // key not found
	}*/
}
