package aufgabe07_2;

import static org.junit.Assert.*;

import org.junit.Test;

public class binarySearchUnitTest {

	@Test
	public void test() {
		int[] case1 = {};
		int[] case2 = {1,2,3};
		final int[] case3 = {1,2,3,4,5};
		int key;
		int outcome;
		
		// Fall 1 (fehlerhaft!): leeres Array -> Key egal
		// fälschlicherweise wird hier 0 statt -1 ausgegeben!
		key = 1;
		/*outcome = binarySearch.binarySearch01(case1, key);
		assertEquals(0,outcome);
		*/
		// Fall 1 (richtig): leeres Array -> Key egal
		/*outcome = binarySearch.binarySearch01(case1, key);
		assertTrue(outcome < 0);
		// assertEquals(0,outcome);
		*/
		// Fall 2: nichtleeres Array, Key nicht enthalten
		key = 5;
		outcome = binarySearch.binarySearch01(case2, key);
		assertTrue(outcome < 0);
		//assertEquals(-1,outcome);
		
		// Fall 3: nichtleeres Array, Key enthalten
		outcome = binarySearch.binarySearch01(case3, key);
		assertEquals(4,outcome);
		
		outcome = binarySearch.binarySearch01(case3, -1);
		assertTrue(outcome < 0);
		
	}

}
