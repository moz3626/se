package Aufgabe07;

import static org.junit.Assert.*;

import org.junit.Test;

public class bubbleSortUnitTest {

	@Test
	public void test() {
		// Arrays mit erwarteten Testergebnissen
		int[] empty = {};
		int[] sorted = {1,2,3};
		final int[] unsorted = {3,2,1};
		
		// Fall 1: leeres Array
		int[] testArray01 = {};
		bubbleSort.bubbleSort01(testArray01);
		assertArrayEquals(empty, testArray01);
		
		// Fall 2: sortiertes Array
		int[] testArray02 = {1,2,3};
		bubbleSort.bubbleSort01(testArray02);
		assertArrayEquals(sorted, testArray02);
		
		// Fall 3: unsortiertes Array
		int[] testArray03 = {3,2,1};
		bubbleSort.bubbleSort01(testArray03);
		assertArrayEquals(sorted, testArray03);
		// assertTrue(sorted.equals(ergebnis03)); // Test eins & zwei klappen; drei noch nicht. egal ob assertEquals(sorted,ergebnis03)[vgl speicheradresse?!S] oder assertTrue
	}

}
