package Aufgabe07;

public class bubbleSort {

	public static void bubbleSort01( int [] array ) {
		int n = array.length;
		boolean swapped = false;
		do {
			swapped = false;
			for( int i = 0; i < n - 1; i++ ) {
				if( array[ i ] > array[ i + 1 ] ) {
					int tmp = array[ i ];
					array[ i ] = array[ i + 1 ];
					array[ i + 1 ] = tmp;
					swapped = true;
				}
			}
			n--;
		} while( n >= 0 && swapped == true );
	}
	
	
}
