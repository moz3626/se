package decl.copy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileInterface {
    void copyFile(File source, File target) throws IOException;
}
