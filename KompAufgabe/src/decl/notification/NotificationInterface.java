package decl.notification;

public interface NotificationInterface {
    void sendMessage(String messageText);
}
