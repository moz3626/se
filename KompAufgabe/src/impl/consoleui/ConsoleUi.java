package impl.consoleui;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import config.KonfigurationStatic;
import decl.copy.FileInterface;
import decl.notification.NotificationInterface;

public class ConsoleUi {
	
    private FileInterface filecomponent;
    
	public ConsoleUi(FileInterface filecomponent){
	    super();
	    this.filecomponent = filecomponent;
	    
	}
    
	public void start(){
		
		
		
		
		Scanner sc = new Scanner(System.in);
		
		File src;
		File ziel;
		
		while(true){
			
			System.out.println("Geben Sie die zu kopierende Datei (Pfad): \n");
			src = new File(sc.nextLine());
			System.out.println("Geben Sie das Ziel ein (Pfad): \n");
			ziel = new File(sc.nextLine());
			
			try {
				filecomponent.copyFile(src, ziel);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
		
		
	
		
		
		
		
	}
}
