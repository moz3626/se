package impl.copy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import decl.copy.FileInterface;
import decl.notification.NotificationInterface;
import impl.notification.FileCNotification;

public class FileCCopy implements FileInterface{

	private NotificationInterface notificationComponent;
	
	public FileCCopy(NotificationInterface notificationComponent) {
		// TODO Auto-generated constructor stub
		super();
		this.notificationComponent = notificationComponent;
		
	}
	
    @Override
    public void copyFile(File source, File target) throws IOException{
    	 InputStream input = null;
    	 OutputStream output = null;
    	 try {
    		         input = new FileInputStream(source);
    		         output = new FileOutputStream(target);
    		         byte[] buf = new byte[1024];
    		         int bytesRead;
    		         while ((bytesRead = input.read(buf)) > 0) {
    		             output.write(buf, 0, bytesRead);
    		         }
    		         notificationComponent.sendMessage("Erfolgreich kopiert\n");
    		     } finally {
    		         input.close();
    		         output.close();
    		     }

    	
        
    }

}
