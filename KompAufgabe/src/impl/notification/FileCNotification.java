package impl.notification;

import decl.notification.NotificationInterface;

public class FileCNotification implements NotificationInterface{

    @Override
    public void sendMessage(String messageText) {
        
    	System.out.println(messageText + "\n");
        
    }

}
