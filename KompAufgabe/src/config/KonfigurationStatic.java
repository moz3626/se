package config;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import decl.copy.FileInterface;
import decl.notification.NotificationInterface;
import impl.consoleui.ConsoleUi;
import impl.copy.FileCCopy;
import impl.notification.FileCNotification;

public class KonfigurationStatic {

	private static KonfigurationStatic instance 	= null;
	private NotificationInterface 		notification			= null;
	private  FileInterface	file	= null;
	private static ConsoleUi consoleui = null;

	/**
	 * Singleton-Pattern. Load one singleton of the configuration object
	 *
	 * @return
	 */

	public static KonfigurationStatic getInstance() {

		if( instance == null ) {
			instance = new KonfigurationStatic();
			instance.initialize();
		}

		return instance;
	}

	/**
	 * Return an instance of the logging object
	 *
	 * @return
	 */

	public NotificationInterface getNotificationInterface() {
		return notification;
	}

	/**
	 * Return an instance of the notification component
	 *
	 * @return
	 */

	public FileInterface getFileInterface() {
		return file;
	}

	/**
	 * Configuration of all components
	 */

	private void initialize() {
		this.notification 			= new FileCNotification();
		this.file 	= new FileCCopy(notification);
		this.consoleui = new ConsoleUi(this.file);
	}
	
	public static void main(String args[]){
	    
	    KonfigurationStatic.getInstance().consoleui.start();
	    
	}
	
}
